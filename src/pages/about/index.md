---
path: '/about'
title: 'Hakkımızda Hayırlısı'
---

``HALBUR`` yani **Hal**ime **Bur**kay muhteşem bir çift. *bilgisayar mühendisi*. Bir birine deliler gibi aşık ve 
anlatamadıkları kadar birbirini çok seven  ``ÇOK``  iki birey.

```
 #include<stdio.h>

 int main() {
   char *name = "HALBU";

   printf("%s", name);
   return 0;
 }
```
    